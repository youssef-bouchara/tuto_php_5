<?php

if($this->request->prefix == 'admin')
{
    $this->layout = 'admin';
    if(!$this->session->isLogged() || $this->session->user('role') != 'admin')
    {
        $this->redirect('users/login');
    }
}


?>