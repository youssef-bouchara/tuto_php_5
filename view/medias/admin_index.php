
<table class="table">
    <thead>
    <tr>
        <th></th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($images as $k => $v): ?>
        <tr>
            <td><img src="<?php echo Router::webroot('img/'.$v->file); ?>" height="100" /></td>
            <td><?php echo $v->name; ?></td>
            <td>
                <a onclick="return confirm('Voulez vous vraiment supprimer cette image ?');" href="<?php echo Router::url('admin/medias/delete/'.$v->id); ?>"> Supprimer</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="page-header">
    <h1>Ajouter une image</h1>
</div>
<form action="<?php Router::url('admin/medias/index/'.$post_id); ?>" method="post" enctype="multipart/form-data">
    <?php echo $this->form->input('file','Image',array(
        "type" => 'file',
        'class' => 'form-control-file')); ?>
    <?php echo $this->form->input('name','Titre'); ?>

    <div class="form-group">
        <input type="submit" class="btn btn-primary " value="Envoyer" />
    </div>
</form>
