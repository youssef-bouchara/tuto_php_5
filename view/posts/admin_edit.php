<div class="page-header">
    <h1>Editer un article</h1>
</div>

<table class="table">
   <form action="<?php echo Router::url('admin/posts/edit/'.$id); ?>" method="post">
       <div class="row">
           <div class="col-md-12">
                   <?php echo $this->form->input('name','titre',array(
                       'class' => 'form-control')); ?>

                   <?php echo $this->form->input('slug','URL',array(
                       'class' => 'form-control')); ?>
               <?php echo $this->form->input('id','hidden'); ?>

                   <?php echo $this->form->input('content','contenu',array(
                       'type' => 'textarea',
                       'class' => 'form-control',
                       'rows' => 5,
                       'cols' => 10)); ?>

                   <?php echo $this->form->input('online','En ligne',array(
                       'type' => 'checkbox',
                       'class' => 'checkbox',
                       'value' => 1)); ?>

               <div class="form-group">
                   <input type="submit" class="btn btn-primary " value="Valider" />
               </div>
           </div>

       </div>
   </form>

</table>