<?php $title_for_layout='Liste ds posts'; ?>
<div class="page-header">
    <h1>Le blog</h1>
</div>
<div class="hero-unit">
    <?php foreach ($posts as $k => $v): ?>
        <h2><?php echo $v->name; ?></h2>
        <?php echo $v->content; ?>
        <p><a href="<?php echo Router::url("posts/view/id:{$v->id}/slug:$v->slug"); ?>">Lire la suite &rarr;</a></p>
    <?php endforeach; ?>
</div>



<nav>
    <ul class="pagination">
        <?php for ($i=1; $i<=$pages; $i++): ?>
        <li <?php if($i==$this->request->page) echo 'class="active"'; ?> ><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>
</nav>