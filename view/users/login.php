<div class="page-header">
    <h1>Login</h1>
</div>

<table class="table">
    <form action="<?php echo Router::url('users/login'); ?>" method="post">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <?php echo $this->form->input('login','Login',array(
                    'class' => 'form-control')); ?>

                <?php echo $this->form->input('password','Mot de passe',array(
                    'class' => 'form-control',
                    'type' => 'password')); ?>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary " value="Connect" />
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </form>

</table>