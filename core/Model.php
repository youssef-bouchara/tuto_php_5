<?php


class Model
{

    static $connection= array();

    public $conf = 'default';

    public $table = false;

    public $db;

    public $primaryKey = 'id';

    public $id;




    public function __construct()
    {


        // J'initialise qulques variables
        if($this->table === false)
        {
            $this->table = strtolower(get_class($this)).'s';
        }

        // Jme connecte à la base
        $conf = Conf::$databases[$this->conf];
       if(isset(self::$connection[$this->conf]))
       {
           $this->db=self::$connection[$this->conf];
           return true;
       }
       try
       {
           $pdo= new PDO('mysql:host='.$conf['host'].';dbname='.$conf['database'].';',$conf['login'],$conf['password'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
           $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
           self::$connection[$this->db] = $pdo;
           $this->db=$pdo;
       }
       catch(PDOException $e)
       {
           if(Conf::$debug>=1)
           {
               die($e->getMessage());
           }
           else
           {
               die('Impossible de se connecter à la base de donnée');
           }
       }


    }

    public function find($req)
    {
        $sql= 'select ';

        if(isset($req['fields']))
        {
            if(is_array($req['fields']))
            {
                $sql .= implode(', ',$req['fields']);
            }
            else
            {
                $sql .= $req['fields'];
            }
        }
        else
        {
            $sql .= '*';
        }

        $sql .= ' from '.$this->table.' as '. get_class($this);

        //construction de la condition
        if(isset($req['conditions']))
        {
            $sql .=' where ';
            if(!is_array($req['conditions']))
            {
                $sql.= $req['conditions'];
            }
            else
            {
                $cond=array();
                foreach ($req['conditions'] as $k=>$v)
                {
                    if(!is_numeric($v))
                    {
                        $v=''.$this->db->quote($v).'';
                    }
                     $cond[]= "$k=$v";
                }
                $sql .=implode(' AND ',$cond);
            }

        }


        if(isset($req['limit']))
        {
            $sql .=' limit '.$req['limit'];

        }

        $pre= $this->db->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_OBJ);
    }

    public function findFirst($req)
    {
        return current($this->find($req));
    }

    public function findCount($condition)
    {
        $res = $this->findFirst(array(
            'fields' => 'count('.$this->primaryKey.') as count',
            'conditions' => $condition
        ));
        return $res->count;
    }

    public function delete($id)
    {
        $sql = "delete from {$this->table} where {$this->primaryKey}=$id";
        $this->db->query($sql);
    }

    public function save($data)
    {
        $key = $this->primaryKey;
        $fields=array();
        $d=array();


        foreach ($data as $k => $v)
        {
            $fields[] = "$k=:$k";
            $d[":$k"] = $v;
        }
        if(isset($data->$key) && !empty($data->$key))
        {
            $sql='update '.$this->table.' set '.implode(',',$fields).' where '.$key.'=:'.$key;
            $this->id = $data->$key;
            $action = 'update';
        }
        else
        {
            if(isset($data->$key))
            {
                unset($data->$key);
            }
            $sql='insert into '.$this->table.' set '.implode(',',$fields);
            $action = 'insert';
        }

        $pre= $this->db->prepare($sql);
        $pre->execute($d);

        if($action == 'insert')
        {
            $this->id=$this->db->lastInsertId();
        }


    }



}

?>