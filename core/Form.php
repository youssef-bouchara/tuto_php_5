<?php

class Form
{
    public $controller;


    public function __construct($controller)
    {
        $this->controller=$controller;
    }

    public function input($name,$label,$options=array())
    {


        if(!isset($this->controller->request->data->$name))
        {
            $value = '';
        }
        else
        {
            $value = $this->controller->request->data->$name;
        }

        if($label == 'hidden')
        {
            if(isset($options['value']))
            {
                return '<input type="hidden" name="'.$name.'" value="'.$options['value'].'">';
            }
            else
            {
                return '<input type="hidden" name="'.$name.'" value="'.$value.'">';
            }

        }

        $html='<div class="form-group"><label for="input'.$name.'">'.$label.'</label><div class="form-group">';

        $attr =' ';
        foreach ($options as $k => $v)
        {
            if($k!='type')
            {
                $attr .= " $k=\"$v\"";
            }
        }

        if(!isset($options['type']))
        {
            $html.='<input type="text" id="input'.$name.'" name="'.$name.'"'.$attr.' placeholder="'.$label.'" '.(empty($value)?"":"value={$value}").' />';
        }
        elseif($options['type'] == 'textarea')
        {
            $html.='<textarea id="input'.$name.'" name="'.$name.'"'.$attr.'>'.$value.'</textarea>';
        }
        elseif($options['type'] == 'checkbox')
        {
            $html.='<input type="hidden" name="'.$name.'" value="0" /><input type="checkbox" name="'.$name.'"'.$attr.' '.(empty($value)?"":"checked").' />';
        }
        elseif($options['type'] == 'file')
        {
            $html.='<input type="file" id="input'.$name.'" name="'.$name.'"'.$attr.' placeholder="'.$label.'" />';
        }
        elseif($options['type'] == 'password')
        {
            $html.='<input type="password" id="input'.$name.'" name="'.$name.'"'.$attr.' placeholder="'.$label.'" />';
        }

        $html.='</div></div>';
        return $html;

    }
}

?>