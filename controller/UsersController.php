<?php

class UsersController extends Controller
{

    function login()
    {
        if($this->request->data)
        {
            $data = $this->request->data;
            $data->password = sha1($data->password);
            $this->loadModel('User');
            $user = $this->User->findFirst(array(
                'conditions' => array('login' => $data->login,'password' => $data->password)
                ));
            if(!empty($user))
            {
                $this->session->write('User',$user);

            }
            $this->request->data->password ='';
        }

        if($this->session->isLogged())
        {
            if($this->session->user('role') == 'admin')
            {
                $this->redirect('cockpit');
            }
            else
            {
                $this->redirect('');
            }
        }
    }

    function logout()
    {
        unset($_SESSION['User']);
        $this->session->setFlash('Vous êtes maintenant déconnecté');
        $this->redirect('');
    }

}